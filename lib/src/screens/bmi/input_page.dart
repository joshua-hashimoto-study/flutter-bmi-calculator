import 'package:bmi_calculator/src/enums/gender_enum.dart';
import 'package:bmi_calculator/src/screens/bmi/result_page.dart';
import 'package:bmi_calculator/src/screens/bmi/widgets/bottom_button.dart';
import 'package:bmi_calculator/src/screens/bmi/widgets/icon_content.dart';
import 'package:bmi_calculator/src/services/calculator_brain.dart';
import 'package:bmi_calculator/src/widgets/reusable_card.dart';
import 'package:bmi_calculator/src/widgets/round_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:bmi_calculator/src/utils/constans.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  Gender selectedGender;
  int userHeight = 160;
  int userWeight = 60;
  int userAge = 20;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('BMI CALCULATOR'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Row(
              children: <Widget>[
                maleCard(),
                femaleCard(),
              ],
            ),
          ),
          sliderWidget(),
          Expanded(
            child: Row(
              children: <Widget>[
                weightWidget(),
                ageWidget(),
              ],
            ),
          ),
          calculateButtonWidget(),
        ],
      ),
    );
  }

  Widget maleCard() {
    return Expanded(
      child: ReusableCard(
        backgroundColor: selectedGender == Gender.male ? kActiveCardColor : kInactiveCardColor,
        cardChild: IconContent(
          icon: FontAwesomeIcons.mars,
          label: 'male',
        ),
        onSelectGender: () {
          setState(() {
            selectedGender = Gender.male;
          });
        },
      ),
    );
  }

  Widget femaleCard() {
    return Expanded(
      child: ReusableCard(
        backgroundColor: selectedGender == Gender.female ? kActiveCardColor : kInactiveCardColor,
        cardChild: IconContent(
          icon: FontAwesomeIcons.venus,
          label: 'female',
        ),
        onSelectGender: () {
          setState(() {
            selectedGender = Gender.female;
          });
        },
      ),
    );
  }

  Widget sliderWidget() {
    return Expanded(
      child: ReusableCard(
        backgroundColor: kActiveCardColor,
        cardChild: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'height'.toUpperCase(),
              style: kLabelTextStyle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.alphabetic,
              children: <Widget>[
                Text(
                  '$userHeight',
                  style: kNumberTextStyle,
                ),
                Text('cm'),
              ],
            ),
            SliderTheme(
              data: SliderTheme.of(context).copyWith(
                thumbShape: RoundSliderThumbShape(
                  enabledThumbRadius: 15.0,
                ),
                thumbColor: Color(0xFFEB1555),
                overlayShape: RoundSliderOverlayShape(
                  overlayRadius: 30.0,
                ),
                overlayColor: Color(0x29EB1555),
                activeTrackColor: Colors.white,
                inactiveTrackColor: Color(0xFF8D8E98),
              ),
              child: Slider(
                value: userHeight.toDouble(),
                min: 120.0,
                max: 240.0,
                onChanged: (double val) {
                  setState(() {
                    userHeight = val.round();
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget weightWidget() {
    return Expanded(
      child: ReusableCard(
        backgroundColor: kActiveCardColor,
        cardChild: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'weight'.toUpperCase(),
              style: kLabelTextStyle,
            ),
            Text(
              '$userWeight',
              style: kNumberTextStyle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RoundIconButton(
                  icon: FontAwesomeIcons.minus,
                  onPress: () {
                    setState(() {
                      userWeight--;
                    });
                  },
                ),
                SizedBox(
                  width: 10.0,
                ),
                RoundIconButton(
                  icon: FontAwesomeIcons.plus,
                  onPress: () {
                    setState(() {
                      userWeight++;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget ageWidget() {
    return Expanded(
      child: ReusableCard(
        backgroundColor: kActiveCardColor,
        cardChild: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'age'.toUpperCase(),
              style: kLabelTextStyle,
            ),
            Text(
              '$userAge',
              style: kNumberTextStyle,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RoundIconButton(
                  icon: FontAwesomeIcons.minus,
                  onPress: () {
                    setState(() {
                      userAge--;
                    });
                  },
                ),
                SizedBox(
                  width: 10.0,
                ),
                RoundIconButton(
                  icon: FontAwesomeIcons.plus,
                  onPress: () {
                    setState(() {
                      userAge++;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget calculateButtonWidget() {
    return BottomButton(
      label: 'calculate',
      onPress: () {
        CalculatorBrain calc = CalculatorBrain(height: userHeight, weight: userWeight);

        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ResultPage(
              bmiResult: calc.calculateBMI(),
              resultText: calc.getResult(),
              interpretation: calc.getInterpretation(),
            ),
          ),
        );
      },
    );
  }
}
