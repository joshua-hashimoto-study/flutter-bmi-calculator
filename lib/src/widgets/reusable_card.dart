import 'package:flutter/material.dart';

class ReusableCard extends StatelessWidget {
  ReusableCard({@required this.backgroundColor, this.cardChild, this.onSelectGender});

  final Color backgroundColor;
  final Widget cardChild;
  final Function onSelectGender;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onSelectGender,
      child: Container(
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: cardChild,
      ),
    );
  }
}
